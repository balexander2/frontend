<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/

Route::auth();

Route::get('/thankyou', 'HomeController@index');
Route::get('/', 'Auth\AuthController@getRegister');

Route::get('/passenger', function () {
    return view('passenger');
});
Route::get('/transport', function () {
    return view('transport');
});
Route::get('/city', function () {
    return view('city');
});
Route::get('/social', function () {
    return view('social');
});
