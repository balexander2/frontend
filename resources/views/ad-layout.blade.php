<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/main-layout.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/font-awesome.min.css') }}">
        <title>@yield('title')</title>
    </head>
    <body>

    <header id="navbar">
        <div id="logo">
            <a href="http://www.orbitaline.com/en/index.html"><img src="{{ asset('lib/img/orbita-post-logo.min.png') }}" alt="Logo - Orbita Line"></a>
        </div>

        <nav>
            <a href="/Passenger/">Passenger</a>
            <a href="/Business/">Business</a>
            <a href="/Transport/">Transport operator</a>
            <a href="/City/">City + PTA</a>
            <a href="/Funding/">Funding organization</a>
            <a href="/Technology/">Technology</a>
            <a href="/About/">About</a>
            <a href="/Press/">Press</a>
            <a href="/Contacts/">Contacts</a>
        </nav>

        <div class="linktoaction sub">
            <a href="/signup/">@yield('linktoaction')</a>
        </div>
    </header>
    
    @yield('content')
    
    <footer>
        <hr>
        <nav>
            <a href="/passenger/">Passenger</a>
            <a href="/Business/">Business</a>
            <a href="/transport/">Transport operator</a>
            <a href="/city/">City + PTA</a>
            <a href="/Funding/">Funding organization</a>
            <a href="/Technology/">Technology</a>
            <a href="/About/">About</a>
            <a href="/Press/">Press</a>
            <a href="/Contacts/">Contacts</a>
        </nav>

        <div id="contacts">
            <p>
                <img src="{{ asset('lib/img/eu-flag.png') }}" alt="European Union Flag - Orbita Line"> 
                <img src="{{ asset('lib/img/swedish-flag.png') }}" alt="Swedish Flag - Orbita Line">
                Orbita Line is the project of <a href="skanatek.se">Skanatek AB</a>, Sweden
            </p>
            <p><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:info@skanatek.se">info@skanatek.se</a></p>
            <p><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:+46739040526">+46 73 904 0526</a></p>
            <p>Skanatek AB. Headquarters: Göteborg, Sweden. <a href="http://bolagsverket.se/">Organisation number</a>: 556932-6571. <a href="http://www.skatteverket.se/foretagorganisationer/moms.4.18e1b10334ebe8bc80002497.html">VAT number</a>: SE556932657101.</p>
        </div>

        <div id="social">
            <div id="socicon">
                <a href="mailto:email@orbitaline.com"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                <a href="blog.orbitaline.com"><i class="fa fa-rss" aria-hidden="true"></i></a>
                <a href="http://www.twitter.com/orbitaline"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href="http://www.gplus.com/orbitaline"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                <a href="http://www.facebook.com/orbitaline"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="https://instagram.com/orbitaline"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href="https://www.linkedin.com/company/orbita-line" class="socicon"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
            <p><a href="http://creativecommons.org/licenses/by-sa/4.0/"><img src="{{ asset('lib/img/creative-commons.png') }}" alt="Creative Commons - Orbita Line"></a> This site is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.</p>
            <p id="imgprovided">Images provided by: <a href="https://www.flickr.com/photos/christianhaugen/">Christian Haugen</a>, <a href="https://www.flickr.com/photos/singaporebus/">Singapore Buses</a>, <a href="https://www.flickr.com/photos/sgtransport/">SG transport</a>, <a href="https://www.flickr.com/photos/ekilby/">Eric Kilby</a>, <a href="https://www.flickr.com/photos/itdp/">ITDP</a><p>
            <p>All the information on this site is published with reservation for errors and changes.</p>
        </div>
    </footer>
    <div id="next">
        <a href=@yield('next')><p>Next</p></a>
    </div>

    </body>
</html>
