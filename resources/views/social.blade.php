@extends('ad-layout')
@section('title', 'Social')
@section('linktoaction', 'Increase your investment impact')
@section('content')
    <section id="main">
        <header>
            <h1>The impact from your financial resources will I N C R E A S E .</h1>
            <h2>Orbita Line is a disruptive frugal innovation changing the lives of low- and medium-income families.</h2>
        </header>

        <div id="jumbotron">
            <img src="{{ asset('lib/img/central-african-reunion-bus-inside.min.jpg') }}" alt="Central African Reunion Bus Inside - Orbita Line">
        </div>

        <div class="linktoaction sub">
            <a href="/signup/">Increase your investment impact</a>
        </div>

        <section id="benefits">
            <div>
                <h3>Turns struggling cities into smart cities.</h3>
                <p>Orbita Line brings modern and reliable public transportation to developing cities at a very low cost.</p>
            </div>
            <div>
                <h3>Affordable to anyone.</h3>
                <p>Orbita Line introduces public transportation that is affordable for low-income families.</p>
            </div>
            <div>
                <h3>Works with simple 2G cellular networks.</h3>
                <p>Orbita Line's technology can help transport operators even if they have no access to 3G/4G mobile broadbank infrastructure.</p>
            </div>
            <div>
                <h3>Works with cheap phones.</h3>
                <p>Orbita Line can be used with any of the cheapest smartphones which continue to capture low-end emerging markets at a great speed.</p>
            </div>
        </section>

        <section id="awards" class="wallofimg">
            <h3>International awards and achievements</h3>
            <a href="http://www.venturecup.se/vinnare-hosten-2015/"><img src="{{ asset('lib/img/orbita-line-venture-cup-sweden-winner-badge-fall-2015-sigill.min.png') }}" alt="Venture Cup Sweden Väst - Absolute Winner - Orbita Line"></a>
            <a href="http://www.venturecup.se/vinnare-hosten-2015/"><img src="{{ asset('lib/img/orbita-line-venture-cup-sweden-winner-badge-fall-2015-sigill.min.png') }}" alt="Venture Cup Sweden Väst - People and Society - Orbita Line"></a>
            <a href="http://copernicus-masters.com/"><img src="{{ asset('lib/img/copernicus-masters-satapps-catapult-finalist-badge.min.png') }}" alt="Copernicus Masters Finalist Badge- Orbita Line"></a>
            <a href="http://universityworldcup.com/"><img src="{{ asset('lib/img/university-startup-world-cup-finalist-badge-2015.min.png') }}" alt="University Startup Finalist Badge - Orbita Line"></a>
            <a href="http://esnc.eu/"><img src="{{ asset('lib/img/esnc-finalist-badge-2015.min.png') }}" alt="ESNC Finalist Badge - Orbita Line"></a>
            <a href="http://www.verizon.com/about/portal/powerful-answers/"><img src="{{ asset('lib/img/verizon-finalist-badge-2015.min.png') }}" alt="Verizon Finalist Badge - Orbita Line"></a>
        </section>

        <section id="recognition" class="wallofimg">
            <h3>Internationally recognized</h3>
            <a href="http://www.aol.com/article/2015/09/24/verizon-announces-36-finalists-for-the-2015-powerful-answers-awa/21239515/?&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript"><img src="{{ asset('lib/img/aol-logo-black.min.png') }}" alt="AOL - Orbita Line"></a>
            <a href="http://digital.di.se/artikel/regionvinnare-i-venture-cup"><img src="{{ asset('lib/img/di-digital.svg') }}" alt="Dagens Industri Digital - Orbita Line"></a>
            <a href="http://www.bussmagasinet.se/2015/12/goteborgsforetag-prisat-for-uber-for-stadsbussar/"><img src="{{ asset('lib/img/buss-magasinet.min.jpg') }}" alt="Buss Magasinet - Orbita Line"></a>
            <a href="http://www.driva-eget.se/nyheter/affarer/har-ar-affarsideerna-som-tog-hem-storvinsten"><img src="{{ asset('lib/img/driva-eget.min.png') }}" alt="Driva Eget - Orbita Line"></a>
            <a href="http://www.gp.se/ekonomi/1.2925288-goteborgsforetag-skapar-uber-for-bussar"><img src="{{ asset('lib/img/goteborgs-posten.min.png') }}" alt="Göteborgs-Posten - Orbita Line"></a>
            <a href="http://sebgroup.com/sv/press/nyheter/regionvinnare-i-venture-cup"><img src="{{ asset('lib/img/seb.min.png') }}" alt="SEB - Orbita Line"></a>
            <a href="http://www.ekonominyheter.se/nyheter/goteborgsforetag-skapar-uber-for-bussar,306321"><img src="{{ asset('lib/img/ekomi-nyheter.min.png') }}" alt="EkonomiNyheter - Orbita Line"></a>
            <a href="http://www.breakit.se/artikel/2109/lovande-svenska-startups-far-dela-pa-over-en-halv-miljon-kronor"><img src="{{ asset('lib/img/breakit.min.png') }}" alt="Breakit - Orbita Line"></a>
            <a href="http://www.techsite.io/p/199675"><img src="{{ asset('lib/img/techsite.svg') }}" alt="TechSite - Orbita Line"></a>
        </section>

        <section id="partners" class="wallofimg">
            <h3>Partners</h3>
            <a href="http://www.esa.int/"><img src="{{ asset('lib/img/esa.min.png') }}" alt="European Space Agency - Orbita Line"></a>
            <a href="http://almi.se/"><img src="{{ asset('lib/img/almi.min.png') }}" alt="ALMI - Orbita Line"></a>
            <a href="http://www.gbgnfc.se/"><img src="{{ asset('lib/img/nfc.min.png') }}" alt="NyföretagarCentrum Göteborgsregionen - Orbita Line"></a>
            <a href="http://www.connectvast.se/"><img src="{{ asset('lib/img/connect-vast.min.jpg') }}" alt="Connect Väst - Orbita Line"></a>
        </section>

    </section>

    <aside class="linktoaction">
        <h3>Get a detailed white paper about the social outcomes of Orbita Line.</h3>
        <a href="/signup/">Increase your investment impact</a>
    </aside>
    @section('next', '/passenger')
@stop

