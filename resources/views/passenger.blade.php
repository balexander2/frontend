@extends('ad-layout')
@section('title', 'Passenger')
@section('linktoaction', 'Sign up now')
@section('content')
    <section id="main">
        <header>
            <h1>Your personal bus is here. Forget the timetable, you decide when and where to go.</h1>
            <h2>Cheaper than your local public transit. Request a bus to any place in the city, any time.</h2>
        </header>

        <div id="jumbotron">
            <img src="{{ asset('lib/img/image-orbita-line-woman-bus-smartphone.min.jpg') }}" alt="Woman bus smartphone - Orbita Line">
        </div>

        <div class="linktoaction sub">
            <a href="/signup/">Sign up now</a>
        </div>

        <section id="benefits">
            <div>
                <h3>One tap and a bus picks you up.</h3>
                <p>Yes, you tell the bus when you want to ride.</p>
            </div>
            <div>
                <h3>Forget about waiting at the bus stop.</h3>
                <p>Request an Orbita Line with your smartphone while at home or at work. You will be notified when the bus is ready to pick you up.</p>
            </div>
            <div>
                <h3>Get a bus in 15 minutes or it is free.</h3>
                <p>If we are late to pick you up, you ride for free. Can your local public transit do that?</p>
            </div>
            <div>
                <h3>Your will never be late or it is free.</h3>
                <p>We appreciate your time and your journey with us will take no longer than 25 minutes until the final destination, otherwise you do not need to pay for it.</p>
            </div>
            <div>
                <h3>Get the public transit schedule out of your life.</h3>
                <p>With Orbita Line you can count on a bus whenever you need it, disregarding of time of the day and day of the week.</p>
            </div>
            <div>
                <h3>Affordable for everyone.</h3>
                <p> How about going to work, then going for a lunch and back, then picking up kids from school, then going to a gym at the price of a couple of cups of coffee in total?</p>
            </div>
            <div>
                <h3>Free unlimited connections.</h3>
                <p>We do not charge you for changing between buses, you pay only for your trip from A to B.</p>
            </div>
            <div>
                <h3>Works all around the city.</h3>
                <p>Orbita Line creates dynamic pick-up points based on passenger demand. This means that we will pick you up even if there are no bus stops near you.</p>
            </div>
            <div>
                <h3>You are a very important customer.</h3>
                <p>Orbita Line guarantees that a bus will pick you up even if you are a single passenger requesting a bus to the bus stop of your choice.</p>
            </div>
            <div>
                <h3>No crowds in the bus.</h3>
                <p> Orbita Line guarantees that when you summon a bus by phone there will always be an available place for you.</p>
            </div>
            <div>
                <h3>Never miss a connection again.</h3>
                <p> Orbita Line's satellite-driven ICT system guarantees that when you transfer, the transfer will be guaranteed and the transfer duration will be minimal.</p>
            </div>
            <div>
                <h3>Forget about plotting your route and planning connections.</h3>
                <p>Orbita Line does all that work for you - you can just ride the bus and relax. If Orbita Line thinks you need to transfer to another bus, it will help you.</p>
            </div>
            <div>
                <h3>Cashless payments.</h3>
                <p>Your payment is made automatically when you order an Orbita Line bus with your smartphone.</p>
            </div>
            <div>
                <h3>No need to memorize routes and lines numbers.</h3>
                <p>Orbita Line has no line numbers, it just gets you from A to B by your request.</p>
            </div>
            <div>
                <h3>Properly licensed drivers make sure that you have a pleasant trip.</h3>
                <p>All of our drivers are experienced and have all the required training and licenses.</p>
            </div>
            <div>
                <h3>You are insured when you travel with Orbita Line.</h3>
                <p>Orbita Line is a service you can trust, because we have a strict safety, security and insuring policy for our passengers.</p>
            </div>
        </section>

        <section id="awards" class="wallofimg">
            <h3>International awards and achievements</h3>
            <a href="http://www.venturecup.se/vinnare-hosten-2015/"><img src="{{ asset('lib/img/orbita-line-venture-cup-sweden-winner-badge-fall-2015-sigill.min.png') }}" alt="Venture Cup Sweden Väst - Absolute Winner - Orbita Line"></a>
            <a href="http://www.venturecup.se/vinnare-hosten-2015/"><img src="{{ asset('lib/img/orbita-line-venture-cup-sweden-winner-badge-fall-2015-sigill.min.png') }}" alt="Venture Cup Sweden Väst - People and Society - Orbita Line"></a>
            <a href="http://copernicus-masters.com/"><img src="{{ asset('lib/img/copernicus-masters-satapps-catapult-finalist-badge.min.png') }}" alt="Copernicus Masters Finalist Badge- Orbita Line"></a>
            <a href="http://universityworldcup.com/"><img src="{{ asset('lib/img/university-startup-world-cup-finalist-badge-2015.min.png') }}" alt="University Startup Finalist Badge - Orbita Line"></a>
            <a href="http://esnc.eu/"><img src="{{ asset('lib/img/esnc-finalist-badge-2015.min.png') }}" alt="ESNC Finalist Badge - Orbita Line"></a>
            <a href="http://www.verizon.com/about/portal/powerful-answers/"><img src="{{ asset('lib/img/verizon-finalist-badge-2015.min.png') }}" alt="Verizon Finalist Badge - Orbita Line"></a>
        </section>

        <section id="recognition" class="wallofimg">
            <h3>Internationally recognized</h3>
            <a href="http://www.aol.com/article/2015/09/24/verizon-announces-36-finalists-for-the-2015-powerful-answers-awa/21239515/?&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript&ModPagespeed=noscript"><img src="{{ asset('lib/img/aol-logo-black.min.png') }}" alt="AOL - Orbita Line"></a>
            <a href="http://digital.di.se/artikel/regionvinnare-i-venture-cup"><img src="{{ asset('lib/img/di-digital.svg') }}" alt="Dagens Industri Digital - Orbita Line"></a>
            <a href="http://www.bussmagasinet.se/2015/12/goteborgsforetag-prisat-for-uber-for-stadsbussar/"><img src="{{ asset('lib/img/buss-magasinet.min.jpg') }}" alt="Buss Magasinet - Orbita Line"></a>
            <a href="http://www.driva-eget.se/nyheter/affarer/har-ar-affarsideerna-som-tog-hem-storvinsten"><img src="{{ asset('lib/img/driva-eget.min.png') }}" alt="Driva Eget - Orbita Line"></a>
            <a href="http://www.gp.se/ekonomi/1.2925288-goteborgsforetag-skapar-uber-for-bussar"><img src="{{ asset('lib/img/goteborgs-posten.min.png') }}" alt="Göteborgs-Posten - Orbita Line"></a>
            <a href="http://sebgroup.com/sv/press/nyheter/regionvinnare-i-venture-cup"><img src="{{ asset('lib/img/seb.min.png') }}" alt="SEB - Orbita Line"></a>
            <a href="http://www.ekonominyheter.se/nyheter/goteborgsforetag-skapar-uber-for-bussar,306321"><img src="{{ asset('lib/img/ekomi-nyheter.min.png') }}" alt="EkonomiNyheter - Orbita Line"></a>
            <a href="http://www.breakit.se/artikel/2109/lovande-svenska-startups-far-dela-pa-over-en-halv-miljon-kronor"><img src="{{ asset('lib/img/breakit.min.png') }}" alt="Breakit - Orbita Line"></a>
            <a href="http://www.techsite.io/p/199675"><img src="{{ asset('lib/img/techsite.svg') }}" alt="TechSite - Orbita Line"></a>
        </section>

        <section id="partners" class="wallofimg">
            <h3>Partners</h3>
            <a href="http://www.esa.int/"><img src="{{ asset('lib/img/esa.min.png') }}" alt="European Space Agency - Orbita Line"></a>
            <a href="http://almi.se/"><img src="{{ asset('lib/img/almi.min.png') }}" alt="ALMI - Orbita Line"></a>
            <a href="http://www.gbgnfc.se/"><img src="{{ asset('lib/img/nfc.min.png') }}" alt="NyföretagarCentrum Göteborgsregionen - Orbita Line"></a>
            <a href="http://www.connectvast.se/"><img src="{{ asset('lib/img/connect-vast.min.jpg') }}" alt="Connect Väst - Orbita Line"></a>
        </section>
    </section>

    <aside class="linktoaction">
        <h3>Get free rides and early VIP access</h3>
        <a href="/signup/">Sign up now</a>
    </aside>
    @section('next', '/transport')
@stop

